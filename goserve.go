package main

import (
	"flag"
	"log"
	"net/http"
)

func main() {
	var (
		host = flag.String("host", "0.0.0.0", "Host name")
		port = flag.String("port", "8080", "Port number")
		dir  = flag.String("dir", ".", "Local directory to serve files from")
	)
	flag.Parse()
	address := *host + ":" + *port
	log.Fatal(http.ListenAndServe(address, http.FileServer(http.Dir(*dir))))
}
